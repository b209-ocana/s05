const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {
		let isDuplicate = exchangeRates.find((exchangeRate) => {
			return exchangeRate.alias === req.body.alias
		})

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter NAME'
			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter EX'
			})
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter ALIAS'
			})
		}

		if(typeof(req.body.name) !== 'string' || req.body.name === '' || typeof(req.body.ex) !== 'object' || Object.keys(req.body.ex).length === 0 || typeof(req.body.alias) !== 'string' || req.body.alias === ''){
			return res.status(400).send({
				'error' : 'Incorrect input.'
			})
		}

		if(isDuplicate){
			return res.status(400).send({
				'error' : 'Alias already taken.'
			})
		} else {
			return res.status(200).send({
				'success' : 'Success!'
			});
		}

	})

module.exports = router;
