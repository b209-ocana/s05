const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})

	it('test_api_post_currency_is_running', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: 'nuyen',
				name: 'Shadowrun Nuyen',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.notEqual(res.status,400)
				done();	
			})		
	})

	it('test_api_post_currency_returns_400_if_name_is_missing', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: 'nuyen',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_name_is_not_a_string', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: 'nuyen',
				name: true,
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: 'nuyen',
				name: '',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_ex_is_missing', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: 'nuyen',
				name: 'Shadowrun Nuyen'
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_ex_is_not_object', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: 'nuyen',
				name: 'Shadowrun Nuyen',
				ex: true
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_ex_is_empty', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: 'nuyen',
				name: 'Shadowrun Nuyen',
				ex: {}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_alias_is_missing', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				name: 'Shadowrun Nuyen',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_alias_is_not_string', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				name: 'Shadowrun Nuyen',
				alias: true,
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_alias_is_empty', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				name: 'Shadowrun Nuyen',
				alias: '',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})

	it('test_api_post_currency_returns_400_if_alias_is_duplicate', (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				name: 'Shadowrun Nuyen',
				alias: 'php',
				ex: {
					'peso': 0.47,
					'usd': 0.0092,
					'won': 10.93,
					'yuan': 0.065
				}
			})
			.end((err, res) => {
				assert.equal(res.status,400)
				done();
			})
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})

})
